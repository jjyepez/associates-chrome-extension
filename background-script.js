console.log('cargado script de la barra')
var estado = {
    tab: -1,
    enPausa: false,
    accion: 0
}
// --- sucede en la barra de herramientas
chrome.browserAction.onClicked.addListener( clickHandler )
init()

function init(){
    chrome.tabs.query({'active': true, 'windowId': chrome.windows.WINDOW_ID_CURRENT},
        function(tabs){
            estado.tabId = tabs[0].id
        }
    )
    //toggleBadge( estado.tabId , estado.accion, estado.enPausa )
    crearItemsMenu()
}

function clickHandler( tab ){
    estado.tabId = tab.id

    console.log('click en el boton de la barra')

    const msg = {
        text: "togglePause"
    }
    // se recibira en el content-script
    chrome.tabs.sendMessage( estado.tabId, msg )
console.log( estado )

    //const nuevaAccion = estado.accion+1 > 2 ? 0 : estado.accion+1
    //toggleBadge( estado.tabId, nuevaAccion, estado.enPausa )
    togglePause()
}

function togglePause(){
    estado.enPausa = !estado.enPausa
    console.log( estado.tabId, estado )
    toggleBadge( estado.tabId, 0, estado.enPausa )
}

function toggleBadge( tabId, nuevaAccion, enPausa ){
    if( tabId ){
        const msg = {
            text: 'togglePause',
            enPausa: enPausa,
            accion: nuevaAccion
        }    
        chrome.tabs.sendMessage( tabId, msg )
    }
    
    const textos_accion  = ['PR','RV','DS']
    const colores_accion = ['darkred','green','blue']
    const txt_badge = enPausa ? '⏸' : '' //textos_accion[ nuevaAccion ]
    
    //estado.enPausa = !estado.enPausa
    estado.accion = nuevaAccion

    chrome.browserAction.setBadgeText( { text: txt_badge } );
    chrome.browserAction.setBadgeBackgroundColor( { color: colores_accion[ estado.accion ] } )
}

function crearItemsMenu(){
    chrome.contextMenus.removeAll()

    /*chrome.contextMenus.create({
        title: "Pausar",
        contexts: ["browser_action"],
        onclick: function() {
            estado.enPausa = true
        }
    })
    chrome.contextMenus.create({
        title: "Cambiar a modo Preparación",
        contexts: ["browser_action"],
        onclick: function() {
            estado.enPausa = true
        }
    })
    chrome.contextMenus.create({
        title: "Cambiar a modo Revisión",
        contexts: ["browser_action"],
        onclick: function() {
            estado.enPausa = true
        }
    })
    chrome.contextMenus.create({
        title: "Cambiar a modo Descripciones",
        contexts: ["browser_action"],
        onclick: function() {
            estado.enPausa = true
        }
    })

    chrome.contextMenus.create({
        type: "separator",
        contexts: ["browser_action"]
    })*/

    chrome.contextMenus.create({
        title: "Ir al reporte",
        contexts: ["browser_action"],
        onclick: function() {
            chrome.tabs.create({ 
                url: "reporte.html"
            })
        }
    })
}