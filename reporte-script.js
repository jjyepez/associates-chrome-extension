const global = {}

// -- sucede al cargar la pagina del browser
window.onload = function (){
    init()
}

function init(){
    resetearSegunQuery()
    
    const tipo = 'chrome.storage'
    obtenerStorageAssociates( tipo, ( estado ) => {
            console.log('recuperando ', estado)
            const contenedor = document.querySelector('#contenido')
            contenedor.innerHTML = renderizarContenido( estado )
            const version = document.querySelector('.version')
            version.textContent = `v${chrome.app.getDetails().version} BETA`
            addClickListenersCursos()
        }
    )
}

function obtenerStorageAssociates( src = 'local', callback = null ){
    let salida = '-'
    const key = '_at-estado'

    if( src === 'chrome.storage' ){
        salida = chrome.storage.local.get( [ key ], ( cad ) => {
            console.log('storage get 1', cad )
            callback && callback( cad )
        } )
    } else {
        cad = window.localStorage.getItem( key )
        console.log('local get 1', cad )
        callback && callback( salida )
    }
    return salida
}

function renderizarContenido( jsonEstado = {} ){
    let html = ''
    let totalTL = 0
    let totalTA = 0
    let avgPorcProp = 0
    let avgProp = 0

    try{
        estado = jsonEstado['_at-estado']
        for( let curso in estado.cursos ) {
            let css = ''
            if( curso !== estado.lastCurso ) css = 'oculto'
            const currentCurso = estado.lastCurso === curso ? `<font color='green'>⚫</font>` : ''
            html += `<div class="colapsable">
                <div class='titulo-curso' data-curso='${curso}'>
                    ${currentCurso}
                    ${curso} - ${estado.cursos[ curso ]['name']}
                </div>
                <div class="icon"><a title="Ir al curso" target="_blank" href="https://platzi.com/clases/${estado.cursos[ curso ]['name']}">🡥</a></div>`
            if( !currentCurso ){
                html += `<div class="icon"><a title="Eliminar registros\ndel curso" target="_blank" href="#" data-curso="${curso}" class="eliminar-curso">🞩</a></div>`
            }
            html += `</div>`
            html += `<div class='div-clases curso-${curso} ${css}'>`
            html += `<table>`
            html += `<tr>
              <th>#</th>
              <th>Título de la clase</th>
              <!--th>TLD</th-->
              <th title="Duración base de la clase">TL</th>
              <!--th title="Tiempo de la última sesión">TS</th-->
              <th title="Tiempo Dedicado\nen todas las sesiones">TD</th>
              <th title="% de proporción">%</th>
              <th title="Tiempo Dedicado\npor cada min lineal\nde clase">P</th>
            </tr>`

            var sumaTL = 0, sumaTS = 0, sumaTA = 0, n = 0
            for( let clase in estado.cursos[ curso ].clases ) {
              const data = estado.cursos[ curso ].clases
              const currentClass = estado.lastClase === clase ? `<font color='green'>⚫</font>` : ''
              const aux_claseTL = data[clase]['TL'] <= 0 ? 60 : data[clase]['TL']
              html += '<tr>'
                html += `<td>${currentClass} ${data[clase]['orden']}</td>`
                html += `<td> ${clase} - ${data[clase]['title']}</td>`
                html += `<!--td>${data[clase]['TLD']}</td-->`
                html += `<td>${formatoHHMMSS( data[clase]['TL'] )}</td>`
                
                html += `<!--td>${formatoHHMMSS( data[clase]['TS'] )}</td-->`
                
                html += `<td>${formatoHHMMSS( data[clase]['TA'] )}</td>`
                html += `<td>${~~( data[clase]['TA'] / aux_claseTL * 100 )} %</td>`
                html += `<td>${formatoHHMMSS( ( data[clase]['TA']/aux_claseTL ) * 60 )}</td>`
              html += '</tr>'
              sumaTL += data[clase]['TL'] || 0
              sumaTS += data[clase]['TS']
              sumaTA += data[clase]['TA']
              n++
            }
            const aux_sumaTL = sumaTL <= 0 ? 60 : sumaTL
            const porcentajeProporcion = ~~( sumaTA/aux_sumaTL * 100 )
            const proporcion = ( sumaTA/aux_sumaTL ) * 60

            html += `<tr>
              <th colspan='2'>Se han visto ${n} clases</th>
              <!--th>-</th-->
              <th>${formatoHHMMSS( sumaTL )}</th>
              <!--th>${formatoHHMMSS( sumaTS )}</th-->
              <th>${formatoHHMMSS( sumaTA )}</th>
              <th>${ porcentajeProporcion } %</th>
              <th>${formatoHHMMSS( proporcion ) }</th>
            </tr>`

            html += '</table>'
            html += '</div>'

            totalTL += sumaTL
            totalTA += sumaTA  
        }

        avgPorcProp = ~~( ( totalTA / totalTL ) * 100 )
        avgProp = ( totalTA / totalTL ) * 60

        htmlSummary = `
        <h3>Resumen <i style='font-weight:normal'>( Todo el periodo )</i></h3>
        <table width='100%'>
            <tr>
                <td>TOTAL Tiempo Lineal (TL): </td>
                <td>${ formatoHHMMSS( totalTL ) }</td>
            </tr>
            <tr>
                <td>TOTAL Tiempo Dedicado (TD): </td>
                <td>${ formatoHHMMSS( totalTA ) }</td>
            </tr>
            <tr>
                <td>AVG % de Proporción (TL / TD) </td>
                <td>${ ~~avgPorcProp } %</td>
            </tr>
            <tr>
                <td>AVG Proporción (TL : TD) </td>
                <td>
                    <div class="avg-tl-td">
                        <div style="flex:1">${ formatoHHMMSS( avgProp ) }&nbsp;&nbsp;&nbsp;por cada min lineal</div>
                        <div style="width:3rem" title="Cambiar referencia entre\nminutos / horas"><a style="font-size:1rem" href="#">⏲</a></div>
                    </div>
                </td>
            </tr>
        </table>
        <h3>Desglose por cursos</h3>
        `
        html = htmlSummary + html

    } catch(err) {

        html = '<center><br/>Aún no hay datos registrados.<br/>Para ver el reporte debes haber visitado al menos una clase en alguno de los cursos.</center>'

    }

    return html
}

function eliminarCurso( id_curso ){
    // --- INVESTIGAR LA COMUNICACION DESDE EL REPORTE HACIA CONTENT!!
    // --- para pasar los datos que actualicen el estado y el localStorage en content.js
    // --- adicionalmente se tendran que asegurar las eliminaciones del curso tanto en chrome.storage.local como en localStorage de content
    const confirmar = window.confirm(`Confirma la eliminación de los registros para el curso\n${id_curso}` )

    if( confirmar ){
console.log( `borrando curso ${id_curso}` )
        queuePush({ action: 'deleteCourse', payload: { id_curso } } )
        setTimeout( () => {
            window.location.reload()
        }, 2000 )
    }
/*
        chrome.storage.local.get( ['_at-estado'], ( estado ) => {
            delete estado['_at-estado'].cursos[ id_curso ]
            console.log( 'estado', estado )
            chrome.storage.local.set( {'_at-estado': estado['_at-estado'] } )
            chrome.storage.local.get( ['_at-estado'], ( estado ) => {
                console.log( 'nuevo estado', estado )
            })
        })
    }
*/
}

function queuePush( obj ){
    chrome.storage.local.get( ['_at-queue'], ( data ) => {
        console.log( 'cargado queue', data['_at-queue'] )
        let queue = data['_at-queue']
        if( queue == undefined ){
            queue = []
        }
        queue.push( { ...obj, status: 'pendiente' } )
        chrome.storage.local.set( {'_at-queue': queue } )
        chrome.storage.local.get( ['_at-queue'], ( data ) => {
            console.log( 'nuevo queue+', data['_at-queue'] )
        })
    })
}


function searchToObject() {
    // fuente: https://stackoverflow.com/questions/6539761/window-location-search-query-as-json
    var pairs = window.location.search.substring(1).split("&"),
      obj = {},
      pair,
      i;
    for ( i in pairs ) {
      if ( pairs[i] === "" ) continue;
      pair = pairs[i].split("=");
      obj[ decodeURIComponent( pair[0] ) ] = decodeURIComponent( pair[1] );
    }
   return obj;
  }

  function resetearSegunQuery(){
    const query = searchToObject() // -- variables GET a un json!
    if( query.ac === 'reset' ){
        // --- BORRAR TODA LA DATA ALMACENADA
        chrome.storage.local.clear(function() {
            var error = chrome.runtime.lastError
            if (error) {
                console.error(error)
            }
        })
        chrome.storage.local.clear(function() {
            var error = chrome.runtime.lastError
            if (error) {
                console.error(error)
            }
        })
    }
  }

  function formatoHHMMSS( segundos ){
    let salida = ''
    let segundosR = segundos
    const HH = ~~( segundosR / 3600 )
      segundosR -= HH * 3600
      if( HH > 0 ) salida += `0${HH}`.substr(-2)+' h '
    const mm = ~~( segundosR / 60 )
      segundosR -= mm * 60
    const ss = ~~( segundosR )
    salida += `0${mm}`.substr(-2)+' m '+`0${ss}`.substr(-2)+' s'
    return salida
  }

  function addClickListenersCursos(){
    let els = null
    els = document.querySelectorAll( '.titulo-curso' )
    els.forEach( el => {
        el.addEventListener('click', clickCurso.bind(this, el) )
    })
    els = document.querySelectorAll( '.eliminar-curso' )
    els.forEach( el => {
        el.addEventListener('click', ( e ) => {
            e.preventDefault()
            const id_curso = el.getAttribute('data-curso')
            eliminarCurso( id_curso )
        } )
    })
  }

  function clickCurso( el ){
    const id_curso = el.getAttribute('data-curso')
    document.querySelectorAll( `.div-clases` ).forEach( el => {
        el.classList.add('oculto')
    })
    const divClasesClicked = document.querySelector( `.div-clases.curso-${id_curso}`)
    divClasesClicked.classList.remove('oculto')
  }