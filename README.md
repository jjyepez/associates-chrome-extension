# Associates Chrome Extension

Chrome extension for the Associates team.

# TO DOs

# Must have

- 🗸 Revisar los cálculos en el reporte
- 🗸 Mostrar versión resumida de los datos y no todo el desgloce (como alternativa)
- Agregar el dato de No. de visitas a la clase, al registro
- Evaluar solución para detectar el tiempo lineal en los videos de Youtube
- Registrar las fechas de revisión de cada clase / curso
- Generar la extensión empaquetada

# Nice to have 

- Permitir exportar e importar un respaldo de los datos
- Agregar el tipo de clase (Artículo, Video, Youtube, Otros), al registro
- Poder organizar por mes / período de fechas
- Identificar al usuario Associate


# Wish to have

- Agregar el dato de velocidad de reproducción (1x, 1.5x, etc), al registro
- Agregar el tipo de revisión (PRE, REV, DESC, OTRO)
- Considerar otras fases distintas como: Traducción, Estudio, etc.

continuará ...

# Autor

@jjyepez
