var timer = {}
const doc = document.all[0]
var estado = {
	enPausa: false,
	cursos: {},
	lastCurso: null,
	lastClase: null,
	timerSesion: 0,
	tab: -1
}

// -- sucede al cargar la pagina del browser
window.onload = function (){
	init()
}

function init(){
    // const c = document.querySelector('.TimelineNav-button.icon-arrow-right_A')
	// c.click()
	obtenerStorageAssociates( 'chrome.storage', ( data ) => {
		console.log( 'cargado desde chrome.storage', data )
		if( data ){
			console.log('Cargado desde Storage', data)
			//estado = JSON.parse( data )
			//actualizarBadge( estado.enPausa )
		}
		//estado.timerSesion = 0
		//crearIntervalo()
	})
	obtenerStorageAssociates( 'local', ( data ) => {
		console.log( 'cargado desde local', data )
		if( data ){
			estado = JSON.parse( data )
			actualizarBadge( estado.enPausa )
		}
		estado.timerSesion = 0
		crearIntervalo()
	})
}

// espera los mensajes lasnzados desde la barra 
chrome.runtime.onMessage.addListener( gotMessage )

function gotMessage( message, sender, sendrRsponse ){
	console.log( 'Recibido ', message )
	
    if( message.text === "togglePause" ){
		//console.log( message )
		estado.enPausa = message.enPausa
		actualizarBadge( message.enPausa )

		let jsonAlmacenar = {}
			jsonAlmacenar[`_at-estado`] = estado
        almacenarStorageAssociates( jsonAlmacenar , 'chrome.storage', function(){
            //console.log('se guardo info storage')
		})
		almacenarStorageAssociates( jsonAlmacenar , 'local', function(){
            //console.log('se guardo info local')
        })
	}
	if( message.text === "deleteCourse" ){
		console.log( 'eliminar ' + message.id_curso )
	}
}

function actualizarBadge( enPausa = false ){
	const $badge = document.querySelector('#at-badge') || document.createElement('div')
	$badge.setAttribute('id','at-badge')
	let img = '', src = ''
	if( !estado.enPausa && !enPausa ){
		src = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAA21BMVEUAAACWyUGYykCXyUGYyUGXyUGXyUGXyUGVyUOXyUGXyUGXyUCWyUGXxkKXyUGXykGYyUGXyUCXyUGWyUGYykGXzECVx0Cq2mmZzDOWyUGXykCXykCXyUGXyUKXykGYyUGXyEG/3oyYyUHd7cDb7LzK4ZqbyECXyUH///+73IPc7cDf7sXr9drx+ObX6s0zkQCz2HSgzlGn0l79/vur02WMwlX6/fb3+/Dj8NjR56y+3Ymw1m2YykPj8Mzg78bJ4rvW6rTH4pmm0IyTxlp3tlWYyUJmrUBYpS40kQGDBHKjAAAAJ3RSTlMA9ZDzF/z57RJs2slPG8/MubdnWlcnJAYFjW5h3YB3c1Pfz8qvKxyG3tSNAAABnElEQVRIx5WWZ0PCMBCG07RQyt5TxX0VkS3bPf//LxKbKxYuJPH5/D4ll4S7MIJT4vmkG4+7yTwvOUxDopmDHVJeQhG/5hYQLJ5hcupNG6RYxzVZvpKFg5xXaP7UBgV2ej/fioGSWHE3XwQt3s56gu8PV2vVb0RWVbWD/MzvKOvYVl4T+3Pj+2OA57sIr29RI1tH4QRCwQe49bc8ffXvIUpb5DOWVOh99vt7gi3OnINEmL9/94kAjeC+WTKhJ5h3/5gAWImN0AQiSHkIDyNnJoxHwW3flAxmwkKEHFYyE2ZDESozbiYsMcRZ3kjofmCowI6MhBUgSeYaCS+AuCxuJEyHGIprBWQRCiZLwnMLcEnRk8EvS2I8YtFkWwUjnzDAbeWmQnctDq4kFYYdykRcDYcIShzGUqYCXm/m/UfwyF9UjZUgTUBNg7YZJbZDG5mSdjh6smZCrh5pxihomzGSjqGgb/dIUS+0yMhCQTuykLSNgn4oItWzw0KuyiTULqe+crBTri4sSbzhqB4nXoo+TjQ4ZV4Qz58CL9OP/wCayBPPDrJjOgAAAABJRU5ErkJggg==`
	} else {
		src = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAARFAAAERQHLuLr0AAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAg1QTFRFAAAAmcwzqtVVkttJv9+flMlDl8VGm8hDlcpAmcw9l8ZCm8hA////lcZAmMg+lMlDlstBmcxAyuGalslAl8lCl8hBmMpAmMlClslBl8pAlshBl8lAmMlClshAl8lCmMlCl8pBmMhAmMhClslAl8pCl8lClslBmMpAl8pCl8hB2+y8l8hAlslBm8pGmspHmMlB9uzs9uzslslBl8hBl8lA3e3Al8lBl8lCmMlBl8lB3+XCl8lBl8lBl8lBl8lBv96Ml8lBl8lBl8lBl8lBl8lBl8lBlslBl8lB+vX1+fT0+vX19/Lw+PPx+fTz+/b2l8lB+vb1l8lBl8lBl8lBl8lBs2dnl8lB+v32M5EANJEBWKUuZq1Ad7ZVgAAAgAEBgQMDgwYGhAgIhg0NiBERjMJVkSMjk8Zal8lBmDExmMlCmMpDmTMzmzc3nDg4ncxMn81QoM5SoUJCoc5Uos9Upk1NptCMptFdp9FeqdJhqtNkq9Nmr9VssNZusWJis9d0uduAu9yDvt2Ivt2JxoyMxt2Wx+KZyeK7zJiYz+ao0aKi0qql0+iv1ejK1unM1uq02bKy2uvR3O3A3uDC3+HC3+7F4O/G4eLH4e/Z4uLI48bG4/DM5fDW5szM58/P5+XS6vTZ7dvb7end7/fj8fjm8+bm9u3t9u7u9vru9/vx+vz1/f77/v39/v79/v/+////yNXcTAAAAFh0Uk5TAAUGBwgTFhcYGRscHCQlJicoK1BRU1dZWmBmZ2hrbG1ub3R3eICNj5CRr7a3t7i5xsfIycrKy87P0NXY29zd3+vt7vHy8/T19fb29/f39/j4+fr7/P3+/o7gHYkAAAInSURBVEjHldbnV9NQGAbwt2BZLhQEnAUcpa6qIChUoEhbF2Cx1jpSi6ivoKi4JwrWxRRnFQQXDir3b6RZbXOTpjfPh5yTnOd37k1yMwDo5JTY7I6m9vYmh91WnANpYi6tRkWqLGad+sIKD6ritualqJvKXKgZz8ZMrX7+XkyZPUvV/RUu1ElzId1feQx141uj7K/GtLEo5uPjD1172K03RkGiv3g/f+TmXzKuN4RrmdzPEK/PE0JmEUd/JvLj9sVkUWOSwAZx/ykhc4jvSTwvTgXOK8YoF/t5bk0wfDoQoIArVwAVqAE+nTkZUAHcJKw3jwaYezMiZGI6kQexdcWvxFLEFOegzBe+szYGqtnA7F2+UwmQ62cDr4SOPxtKkAn8uSqWisDGBgakkhXsTGC6SyptgXom8EguOcDJBJ7LpUbwMoF/16VSGyMgQ3HANiXy/548pX0UuN/Hp18lPsgnTV9WMXeEUvT3TCJTkUjkXXib6sYlgei33svK9AzWQrEmuDEey/dejk7oMGT7tYCQj5dUgGsBqDIEDgJY0oDHk58vcMGxyZcyMLv1wWtCbnEdhPySAViNAuVrhgHAeqPAVGMQSC9jAwAKfQYBrDIK+E+WMQAFzSnAMxK9wp2dIV8pAEsatEGw81xs29EZogEs2HyCafElZfludzJ426PqBw/QPyfrdsWfDwwPhmhwfIf6lyCryLq1zun1Ouu21x452qrIoZ2L5gE5r4oMAxJuBQAAAABJRU5ErkJggg==`
	}
	img = `<img class='at-badge' src="${src}">`
	$badge.innerHTML = img
	document.querySelector('body').appendChild( $badge )
}

function crearIntervalo(){

	const clase_uri = document.location.href

	const regex_uri = (/https?:\/\/.*?platzi.com\/clases\/([\d]*)?-([^\/]*)\/?([\d]*)?-([^\/]*)\//).exec(clase_uri)

    console.clear()

	const id_curso   = regex_uri[1]
	const name_curso = regex_uri[2]
	estado.lastCurso = id_curso
	
	const id_clase   = regex_uri[3]
	const name_clase = regex_uri[4]
	estado.lastClase = id_clase

	if( !estado.cursos[ id_curso ] ){
		estado.cursos[ id_curso ] = {
			'id'   : id_clase,
			'name' : name_curso,
			'title': '-',
			'badge': '',
			'url'  : clase_uri,
		}
	}

    sesionInterval = setInterval( () => {

		if( estado.enPausa ) {
			console.log( 'Associate - En pausa' )
			return false
		} else {
			console.log( 'Associate - Conteo' )
		}

		estado.timerSesion++;
		const clases_ant = estado.cursos[ id_curso ] ? estado.cursos[ id_curso ].clases : {}

		var tl = 0
		var tlsec = 0
		var orden = 0

		if( doc.querySelector('.TimelineNav-material.is-active') ){ 
			orden = parseInt( doc.querySelector('.TimelineNav-material.is-active').textContent )
		}
		if( doc.querySelector('.vjs-duration-display') ){
			tl = doc.querySelector('.vjs-duration-display').textContent.replace('Duration Time', '').trim()
			const arrtl = tl.split(':')
				  tlsec = parseInt( arrtl[0] * 60 ) + parseInt( arrtl[1] )
		}

		var title = ''
		if( doc.querySelector('.Description-title') ) title = doc.querySelector('.Description-title').textContent
		if( doc.querySelector('.Discussion-title') ) title = doc.querySelector('.Discussion-title').textContent
		if( doc.querySelector('.MaterialTabs-title') ) title = doc.querySelector('.MaterialTabs-title').textContent

		title = title.replace(/[\n\r\t]?/g, '').trim()

		if( clases_ant ){
			if( typeof( clases_ant ) !== 'object' ){
				json_clases = JSON.parse( clases_ant )
			} else {
				json_clases = clases_ant
			}

			//estado.clases = json_clases
			if( !estado.cursos[ id_curso ] ) estado.cursos[ id_curso ] = {}
			estado.cursos[ id_curso ].clases = json_clases

			if( !json_clases[ id_clase ] ) {
				json_clases[ id_clase ] = {
					orden: orden,
					title: title,
					TA: 0,	
					TS: 0,
					TL: 0,
					TLD: ''
				}
			}
			// ---- MEJORAR ESTO
			json_clases[ id_clase ].title = title

			ta = json_clases[ id_clase ][ 'TA' ]
			ts = json_clases[ id_clase ][ 'TS' ]
		} else {
			estado.cursos[ id_curso ].clases = {}
			ta = 0
			ts = 0
		}
		ts = estado.timerSesion
		ta += 1

		const infoClase = {
			orden: orden,
			title: title,
			TL: 0,
			TLD: '0',
			TA: ta,
			TS: ts
		}
		if( tlsec > 0 ){
			infoClase.TL = tlsec
			infoClase.TLD = tl
		}

		// -- estado.clases[ id_clase ] = infoClase // --- refactorizado

		if( !estado.cursos[ id_curso ].clases ) estado.cursos[ id_curso ].clases = {}
		estado.cursos[ id_curso ].clases[ id_clase ] = infoClase

		queueProcess( task => {
			const { action, payload, status } = task
			let newStatus = false
			switch( action ) {
				case 'deleteCourse':
					delete estado.cursos[ payload.id_curso ]
					console.log('queue', estado)
					newStatus = true
				break;
				default:
			}
			return newStatus
		} ) // --- procesar tareas en la cola

		let jsonAlmacenar = {}
			jsonAlmacenar[`_at-estado`] = estado
        almacenarStorageAssociates( jsonAlmacenar , 'chrome.storage', function(){
            //console.log('se guardo info storage')
		})
		almacenarStorageAssociates( jsonAlmacenar , 'local', function(){
            //console.log('se guardo info local')
        })
	},1000)
}

function queueProcess( logic = null ){
	chrome.storage.local.get( ['_at-queue'], ( data ) => {
		let queue = data['_at-queue']
		queue.forEach( (el, i) => {
			if( logic !== null && logic( queue[i] ) === true ){
				queue[i].status = 'procesada'
			} else {
				queue[i].status = 'error'
			}
		})
		queue = queue.filter( el => {
			return el.status !== 'procesada'
		})
		almacenarStorageAssociates( { '_at-queue': queue }, 'chrome.storage' )
	})
}

function almacenarStorageAssociates( obj, dest = 'local', callback = null ){
    if( dest === 'chrome.storage' ){
        chrome.storage.local.set( obj, callback )
    } else {
        const obj_aux = obj
        for( item in obj ){
            if( typeof( obj[item] ) == 'object' ){
                obj[item] = JSON.stringify( obj[item] )
            }
            window.localStorage.setItem( item, obj[item] )
            callback
        }
    }
}

function obtenerStorageAssociates( src = 'local', callback = null ){
    let salida = '-'
	const key = '_at-estado'

    if( src === 'chrome.storage' ){
        salida = chrome.storage.local.get( [ key ], ( cad ) => {
            console.log('storage get 1', cad )
			callback && callback( cad )
        } )
    } else {
        salida = window.localStorage.getItem( key )
        console.log('local get 1', salida )
        callback && callback( salida )
    }
    return salida
}
